#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from math import floor
from pathlib import Path
from typing import List


def read_input_into_memory(file_path) -> str:
    """
    Open a file as read only and return as a string.
    Strips any whitespace from either end of the string.
    """
    with open(file_path, "r") as input_file:
        input_Str = input_file.read().strip()
    return input_Str


def string_to_list_of_strings(input_str, split_char) -> List[str]:
    """
    Return a list of strings, new string each time \n is seen.
    """
    return [x for x in input_str.split(split_char)]


def convert_list_of_str_to_list_of_int(list_of_strings) -> List[int]:
    """
    Accepts a list of strings, returns a list of ints.
    """
    return list(map(int, list_of_strings))


def fuel_given_mass(mass) -> int:
    fuel = floor(mass / 3) - 2
    return fuel


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")
    INPUT_PATH = Path.cwd() / "./data/day1_input.txt"
    assert INPUT_PATH.exists()
    module_masses = read_input_into_memory(INPUT_PATH)
    module_masses = string_to_list_of_strings(module_masses, "\n")
    module_masses = convert_list_of_str_to_list_of_int(module_masses)

    # Part 1
    # To find the fuel required for a module:
    #   - take its mass,
    #   - divide by three,
    #   - round down,
    #   - and subtract 2.
    # Answer: Total fuel required.
    module_fuels = []
    for x in module_masses:
        module_fuels.append(fuel_given_mass(x))
    total_fuel = sum(module_fuels)

    print("Part 1: Total Fuel Required: " + str(total_fuel))

    # Part 2
    # What is the sum of the fuel requirements for all of the modules on your
    # spacecraft when also taking into account the mass of the added fuel?
    for i in range(len(module_masses)):
        fuel_fuel_need = module_fuels[i]
        while True:
            fuel_fuel_need = fuel_given_mass(fuel_fuel_need)
            if fuel_fuel_need <= 0:
                break
            else:
                total_fuel += fuel_fuel_need

    print("Part 2: Total Fuel Required Incluing Fuel Fuel: " + str(total_fuel))
