#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def get_solution_min_max(min_max_str) -> str:
    """
    Given a string of the form: 12345-67890
    where the characters before the dash define min value
    and the characters after the dash define the max value
    return them as integers
    """
    input_list = [x for x in min_max_str.split("-")]
    return input_list[0], input_list[1]


def solve(range_start, range_end):
    """
    Given the values to start and stop searching between
    Find how many different passwords within the range:
    - are a six-digit number.
    - have a value within the range given in your puzzle input
    - digits that never decrease when analysed left to right

    Part 1:
    - have two adjacent digits the same

    Part 2:
    - have two adjacent matching digits that are not part
        of a larger group of matching digits.
    """
    checked_start = 0
    checked_end = 0

    if len(range_start) >= 7:
        checked_start = "999999"
    elif len(range_start) <= 5:
        checked_start = "100000"
    else:
        checked_start = range_start

    if len(range_end) >= 7:
        checked_end = "999999"
    elif len(range_end) <= 5:
        checked_end = "100000"
    else:
        checked_end = range_end

    print("Checked Start and End: ", checked_start, checked_end)

    potential_passwords = []

    # PART 1
    for password in range(int(checked_start), (int(checked_end) + 1)):
        two_adjacent_match = False
        digits_never_decrease = True
        password_str = str(password)

        for x in range(5):
            # Test for two match
            if password_str[x] == password_str[x + 1]:
                two_adjacent_match = True
            else:
                pass

            # Test for always ascending
            if password_str[x] > password_str[x + 1]:
                digits_never_decrease = False
            else:
                pass

        if two_adjacent_match and digits_never_decrease:
            potential_passwords.append(password)

    print("Part 1: Number of potential passwords: " + str(len(potential_passwords)))

    # PART 2
    part2_passwords = []

    # For each potential password count the number of appearances of each character#
    for password in potential_passwords:
        count = {}
        for char in str(password):
            count[char] = str(password).count(char)
        # If a character only appears two times, then that password is valid
        if 2 in count.values():
            part2_passwords.append(password)

    print("Part 2: Number of passwords: " + str(len(part2_passwords)))


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")
    puzzle_input = "130254-678275"

    start, end = get_solution_min_max(puzzle_input)

    solve(start, end)
