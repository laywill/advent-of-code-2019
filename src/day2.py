#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pathlib import Path

from day1 import convert_list_of_str_to_list_of_int
from day1 import read_input_into_memory
from day1 import string_to_list_of_strings

if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")
    INPUT_PATH = Path.cwd() / "./data/day2_input.txt"
    assert INPUT_PATH.exists()
    data_ip = read_input_into_memory(INPUT_PATH)
    data_ip = string_to_list_of_strings(data_ip, ",")
    data_ip = convert_list_of_str_to_list_of_int(data_ip)

    # Part 1
    # Restore the gravity assist program (your puzzle input) to the "1202
    # program alarm" state it had just before the last computer caught fire.
    # To do this, before running the program, replace position 1 with the
    # value 12 and replace position 2 with the value 2.
    # What value is left at position 0 after the program halts?
    data = data_ip.copy()

    data[1] = 12
    data[2] = 2

    index = 0

    while True:
        if data[index] == 1:
            data[data[index + 3]] = data[data[index + 1]] + data[data[index + 2]]
            index += 4

        elif data[index] == 2:
            data[data[index + 3]] = data[data[index + 1]] * data[data[index + 2]]
            index += 4

        elif data[index] == 99:
            print("Opcode 99 - halting.")
            break

        else:
            print("Unknown Opcode!")
            print("Inspect opcode: ", data[index])
            break

    print("Part 1: Value left at position 0 is:", data[0])

    # Part 2
    # Determine what pair of inputs produces the output 19690720.
    # The inputs should still be provided to the program by replacing the
    # values at addresses 1 and 2, just like before. In this program, the
    # value placed in address 1 is called the noun, and the value placed in
    # address 2 is called the verb.
    for noun in range(100):
        for verb in range(100):
            data = data_ip.copy()

            data[1] = noun
            data[2] = verb

            index = 0

            while True:
                if data[index] == 1:
                    data[data[index + 3]] = (
                        data[data[index + 1]] + data[data[index + 2]]
                    )
                    index += 4

                elif data[index] == 2:
                    data[data[index + 3]] = (
                        data[data[index + 1]] * data[data[index + 2]]
                    )
                    index += 4

                elif data[index] == 99:
                    # print("Opcode 99 - halting.")
                    break

                else:
                    # print("Unknown Opcode!")
                    # print("Inspect opcode: ", data[index])
                    break

            if data[0] == 19690720:
                answer = 100 * noun + verb
                print(
                    "Part 2: To leave value 19690720 in position 0 use:\n",
                    "Noun: " + str(noun) + "\n",
                    "Verb: " + str(verb) + "\n",
                    "Answer: " + str(answer) + "\n",
                )
                exit()
            else:
                pass
