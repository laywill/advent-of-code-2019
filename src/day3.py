#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pathlib import Path
from typing import List


def read_file_as_list(file_path) -> List[str]:
    """
    Open a file as read only and return a list of list of strings
    """
    with open(file_path, "r") as input_file:
        list_of_str = []
        for line in input_file:
            list_of_str.append(line.split(","))
    return list_of_str


def find_points_from_instructions(instructions_list) -> List[tuple]:
    """
    Given a lit of instructions in the form:
    ['R75','D30','L83','U83'...]
    Return a list of tuples, giving all unique locations
    that are visited.
    """
    # Initialise current location to 0,0
    location_x = 0
    location_y = 0

    # Use a dictionary to decode directions to vectors
    directions = {"R": (1, 0), "L": (-1, 0), "U": (0, 1), "D": (0, -1)}

    path_history = {}
    step = 0

    for instruction in instructions_list:
        # Generate constants to determine step direction
        dx, dy = directions[instruction[0]]

        # Iterate for the magnitude of the instruction.
        for _ in range(int(instruction[1:])):
            # Update the location with step direction
            location_x += dx
            location_y += dy
            step += 1

            # If novel location, store it to create a history
            # List lookup takes O(n)
            # Dict lookup takes O(1) so is much faster
            if (location_x, location_y) not in path_history.keys():
                path_history[(location_x, location_y)] = step

    return path_history


def solve(instructions):
    """
    Solve the Day3 problem given a list of instructions in the form:
    ['R75','D30','L83','U83'...] for each wire
    """
    wire1_path = find_points_from_instructions(instructions[0])
    wire2_path = find_points_from_instructions(instructions[1])

    # Generate a list of tuples, where the tuple appears in the
    # wire1_path dict AND the wire2_path dict
    intersecting_points = [point for point in wire1_path if point in wire2_path]

    solve_part_1(intersecting_points)
    solve_part_2(wire1_path, wire2_path, intersecting_points)


def solve_part_1(intersects):
    """
    From a dictionary of instersecting points,
    find the intersection point closest to the central port
    using the Manhattan Distance
    """
    manhattan_to_nearest_intersecting_point = min(
        abs(x) + abs(y) for (x, y) in intersects
    )
    print(
        "Part 1:\n\tthe Manhattan distance from (0,0)",
        "to the closest intersection is:\n",
        str(manhattan_to_nearest_intersecting_point),
    )


def solve_part_2(wire1_points, wire2_points, intersects):
    """
    From a dictionary of intersecting points,
    find the intersection point closest to the central port.
    Use the Manhattan Distance where the sum of both wires' steps is lowest
    """
    manhattan_to_min_steps_intersecting_point = min(
        wire1_points[point] + wire2_points[point] for point in intersects
    )
    print(
        "Part 2:\n\tthe Manhattan distance from (0,0)",
        "to the intersection the smallest total number of steps away is:\n",
        str(manhattan_to_min_steps_intersecting_point),
    )


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")
    # INPUT_PATH = Path.cwd() / "./data/day3_test1.txt"
    # INPUT_PATH = Path.cwd() / "./data/day3_test2.txt"
    INPUT_PATH = Path.cwd() / "./data/day3_input.txt"
    assert INPUT_PATH.exists()
    list_of_instructions = read_file_as_list(INPUT_PATH)

    solve(list_of_instructions)
